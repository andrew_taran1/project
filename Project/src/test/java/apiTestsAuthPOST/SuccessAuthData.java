package apiTestsAuthPOST;

import java.util.ArrayList;

public class SuccessAuthData {
    private Integer userId;
    private String username;
    private String token;
    private ArrayList<String> roles;

    public SuccessAuthData(Integer userId, String username, String token, ArrayList<String> roles) {
        this.userId = userId;
        this.username = username;
        this.token = token;
        this.roles = roles;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public ArrayList<String> getRoles() {
        return roles;
    }

    public void setRoles(ArrayList<String> roles) {
        this.roles = roles;
    }
}